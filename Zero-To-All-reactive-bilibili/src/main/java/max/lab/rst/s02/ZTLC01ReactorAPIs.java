package max.lab.rst.s02;

import lombok.var;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Scheduler;
import reactor.core.scheduler.Schedulers;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Stream;

/***
 * Reactor的API非常丰富和功能齐全，大家在coding的时候要搜索和查阅资料，尽量用这些API来实现程序的流转/返回/异常处理等。
 */
public class ZTLC01ReactorAPIs {

    public static void main(String[] args) {
        Scheduler s = Schedulers.newParallel("parallel-scheduler", 4);

        final Flux<String> flux = Flux
                .range(1, 2)
                .map(i -> 10 + i)
                .publishOn(s)
                .map(i -> "value " + i);

        new Thread(() -> flux.subscribe(System.out::println)).start();
    }

}
