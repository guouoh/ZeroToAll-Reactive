package com.cg.webflux;

import com.cg.webflux.filter.ZtlFilter;
import reactor.core.publisher.Mono;

import java.util.List;

/**
 * @author shuukusunoki
 */
public class DefaultFilterChain implements FilterChain {
    private final int index;

    private final List<ZtlFilter> filters;

    DefaultFilterChain(List<ZtlFilter> filters) {
        this.filters = filters;
        this.index = 0;
    }

    private DefaultFilterChain(DefaultFilterChain parent, int index) {
        this.filters = parent.getFilters();
        this.index = index;
    }

    public List<ZtlFilter> getFilters() {
        return filters;
    }

    @Override
    public Mono<Void> filter(FilterContext filterContext) {
        return Mono.defer(() -> {
            if (this.index < filters.size()) {
                ZtlFilter filter = filters.get(this.index);
                DefaultFilterChain chain = new DefaultFilterChain(this,
                        this.index + 1);
                return filter.filter(filterContext, chain);
            } else {
                return Mono.empty(); // complete
            }
        });
    }
}
