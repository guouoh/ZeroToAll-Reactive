package com.cg.webflux;

import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

public interface FilterChain {
    /**
     * Delegate to the next {@code WebFilter} in the chain.
     *
     * @param filterContext the current server exchange
     * @return {@code Mono<Void>} to indicate when request handling is complete
     */
    Mono<Void> filter(FilterContext filterContext);
}
