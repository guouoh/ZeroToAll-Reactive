package com.cg.webflux.filter;

import com.cg.webflux.FilterChain;
import com.cg.webflux.FilterContext;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

public interface ZtlFilter {
    /**
     * 过滤器接口
     *
     * @param filterContext filterContext
     * @param chain         chain
     * @return Mono
     */
    Mono<Void> filter(FilterContext filterContext, FilterChain chain);
}
