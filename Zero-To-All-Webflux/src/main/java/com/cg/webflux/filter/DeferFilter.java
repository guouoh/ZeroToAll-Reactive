package com.cg.webflux.filter;

import com.cg.webflux.FilterChain;
import com.cg.webflux.FilterContext;
import org.springframework.core.annotation.Order;
import reactor.core.publisher.Mono;

/**
 * @author shuukusunoki
 */
@Order(1)
public class DeferFilter implements ZtlFilter {
    @Override
    public Mono<Void> filter(FilterContext filterContext, FilterChain chain) {
        System.out.println("执行: " + this.getClass().getSimpleName() + " ,顺序:" + this.getClass().getAnnotation(Order.class).value());
        return chain.filter(filterContext)
                .doOnError(throwable -> filterContext.cleanUp())
                .then(Mono.defer(() -> {
                    System.out.println(DeferFilter.class.getSimpleName() + "的Mono.defer中执行");
                    filterContext.concat("work", "defer");
                    return doSomethingReturnVoid(filterContext);
                }));
    }

    private Mono<Void> doSomethingReturnVoid(FilterContext filterContext) {
        System.out.println("在defer中做一些其他的操作");
        return Mono.empty();
    }
}
