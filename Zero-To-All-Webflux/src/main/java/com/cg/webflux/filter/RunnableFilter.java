package com.cg.webflux.filter;

import com.cg.webflux.FilterChain;
import com.cg.webflux.FilterContext;
import org.springframework.core.annotation.Order;
import reactor.core.publisher.Mono;

import java.util.concurrent.TimeUnit;

@Order(0)
public class RunnableFilter implements ZtlFilter {
    @Override
    public Mono<Void> filter(FilterContext filterContext, FilterChain chain) {
        System.out.println("执行: " + this.getClass().getSimpleName() + " ,顺序:" + this.getClass().getAnnotation(Order.class).value());
        return chain.filter(filterContext)
                .then(Mono.fromRunnable(() -> {
                    System.out.println(RunnableFilter.class.getSimpleName() + "的Mono.runnable中执行");

                    try {
                        System.out.println(RunnableFilter.class.getSimpleName() + " ,阻塞执行3s");
                        TimeUnit.SECONDS.sleep(3);
                        System.out.println(RunnableFilter.class.getSimpleName() + " ,阻塞结束");
                    } catch (InterruptedException e) {
                    }
                    filterContext.concat("name", "runnable");
                }));
    }
}
