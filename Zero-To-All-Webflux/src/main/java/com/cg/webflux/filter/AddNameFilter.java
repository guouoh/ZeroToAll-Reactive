package com.cg.webflux.filter;

import com.cg.webflux.FilterChain;
import com.cg.webflux.FilterContext;
import org.springframework.core.annotation.Order;
import reactor.core.publisher.Mono;

/**
 * 增加名字
 *
 * @author shuukusunoki
 */
@Order(3)
public class AddNameFilter implements ZtlFilter {
    @Override
    public Mono<Void> filter(FilterContext filterContext, FilterChain chain) {
        System.out.println("执行: " + this.getClass().getSimpleName()  + " ,顺序:" + this.getClass().getAnnotation(Order.class).value());
        filterContext.put("name", this.getClass().getSimpleName());
        return chain.filter(filterContext);
    }
}
