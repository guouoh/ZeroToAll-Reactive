package com.cg.webflux;

import com.cg.webflux.filter.*;
import org.springframework.core.annotation.AnnotationAwareOrderComparator;

import java.util.ArrayList;
import java.util.List;

public class Client {
    public static void main(String[] args) {
        List<ZtlFilter> filterList = new ArrayList<>();
        filterList.add(new AddAgeFilter());
        filterList.add(new AddWorkFilter());
        filterList.add(new AddNameFilter());
        filterList.add(new DeferFilter());
        filterList.add(new RunnableFilter());
        filterList.add(new Defer1Filter());

        AnnotationAwareOrderComparator.sort(filterList);

        DefaultFilterChain defaultFilterChain = new DefaultFilterChain(filterList);
        FilterContext context = new FilterContext();

        defaultFilterChain.filter(context)
                .subscribe(System.out::println);

        System.out.println();
        System.out.println("过滤器执行结束,打印Context中的内容");

        context.prinnt();
    }
}
