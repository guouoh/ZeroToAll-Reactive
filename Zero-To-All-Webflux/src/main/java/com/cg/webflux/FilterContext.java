package com.cg.webflux;

import java.util.HashMap;
import java.util.Map;

public class FilterContext {
    private Map<String, String> contextValue = new HashMap<>();

    public void put(String key, String value) {
        contextValue.put(key, value);
    }

    public void concat(String key, String content) {
        final String old = contextValue.get(key);
        contextValue.put(key, old + "#" + content);
    }

    public void cleanUp() {
        contextValue.clear();
    }


    public void prinnt() {
        contextValue.forEach((key, value) -> {
            System.out.printf("%s:%s%n", key, value);
        });
    }
}
